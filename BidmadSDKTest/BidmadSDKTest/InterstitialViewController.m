//
//  InterstitialViewController.m
//  BidmadSDKTest
//
//  Created by 김선정 on 2017. 9. 13..
//  Copyright © 2017년 김선정. All rights reserved.
//

#import "InterstitialViewController.h"

@interface InterstitialViewController (){
    BIDMADInterstitial* interstitial;
}

@end


@implementation InterstitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[BIDMADSetting sharedInstance]setInterstitialZoneID:@"__ZONE_ID__"];
    [[BIDMADSetting sharedInstance]setIsDebug:YES];
    interstitial = [[BIDMADInterstitial alloc]init];
    [interstitial setParentViewController:self];
    [interstitial setDelegate:self];
//    [interstitial directShowInterstitialView];   //바로 호출
    [interstitial loadInterstitialView];
}

#pragma mark - 전면광고 로드 완료 후 호출
- (void)BIDMADAdError:(BIDMADInterstitial *)core code:(NSString *)error
{
    NSLog(@"error L: %@",error);
}

#pragma mark - 전면광고 닫힐 시 호출
- (void)BIDMADInterstitialClose:(BIDMADInterstitial *)core
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

#pragma mark - 전면광고 노출 시 호출
- (void)BIDMADInterstitialShow:(BIDMADInterstitial *)core
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

#pragma mark - 전면광고 에러발생 시 호출
-(void)BIDMADInterstitialLoad:(BIDMADInterstitial *)core
{
    [interstitial showInterstitialView];
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
